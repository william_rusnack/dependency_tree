# dependency_tree
Builds a dependency tree of a file's local dependencies.

[![Build Status](https://travis-ci.org/BebeSparkelSparkel/dependency_tree.svg?branch=master)](https://travis-ci.org/BebeSparkelSparkel/dependency_tree)

Uses a pure node structure without a tree object.  
Does not allow cycles in tree.  
node id (node.str property) is based on strings.  


## node Properties
node.**parent** - the parent of the node. If root, is `null`.
node.**str** - the id that is used to compare this node to all other nodes.
node.**children** - an Array of this node's childe nodes.

## node Methods
node.**get_root**()
Returns the root node that this node is under.

node.**add_child**(str)
Adds a child node to this node with an id of *str*.
Does not allow an ancestor to have the same node.str.
Returns child node.

node.**add_child_node**(child_node)
Adds a new child node to this node.
child_node must not already have a parent.
child_node cannot be added to a node where it will have an ancestor with the same str property.
Returns this node not child_node.

node.**in_bloodline**(ancestor)
Returns true if this node.str has an ancestor with ancestor as its str property else false.

node.**is_ancestor_of**(str)
Returns true it this node has a decendant with node.str === str.
Does not include this node in the search.

node.**contains**(str)
Returns true it this node is or has a decendant with node.str === str else false.
Like is node.is_ancestor_of but includes itself.

node.**root_contains**(str)
Checks if the tree that this node is in contains a node with node.str === str.
If it does returns true else false.

node.**num_nodes**()
Returns the number of nodes in the subtree with this node as its root.

node.**flatten**(container=[])
Returns all the node.str in an Array.
Order is not defined.

node.**to_string**(indent_level=0, indent_by=2)
Returns a string that displays the tree structure.
**indent_level** - the starting number of indent spaces
**indent_by** - the number of spaces each tree level is more indented by

## Build Dependency Tree
async function **dependency_tree**(entry_file, {max_depth=100})
Builds the local dependency tree.
*entry_file* - path to the root file
*max_depth* - maximum number of depencency levels that will be added to the tree

## Sort Test Run Order
function **sort_test_order**(src_deptree)
Returns an array with the order of first to last test run order.
*src_deptree* - is the tree returned by the dependency_tree function

## To Do
- Use a abstract syntax tree parser to define the tree structer instead of regex
